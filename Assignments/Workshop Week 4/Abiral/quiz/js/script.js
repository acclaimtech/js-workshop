var settings = {
  count: 0,
  length: 0,
  total: 0,
  timer: 0,
  newTimer: 0,

  init: function() {
    this.total = 0;
    this.count = 0;
    this.length = questions.length;
    this.timer = 60,
      this.newTimer = 60;
  },

  startRound: function() {
    this.printQuestion(questions[this.count]);
    this.startTimer();
  },

  resetTimer: function() {
    this.timer = this.newTimer;
  },

  nextQuestion: function() {
    this.count++;
    this.printQuestion(questions[this.count]);
    this.resetTimer();
  },

  startTimer: function() {
    $('#restart').hide();
    var that = this;
    var timer = setInterval(function() {
      $('#timer p').html(that.timer);
      if ((that.timer == 0) && (that.count != that.length)) {
        that.printQuestion(questions[that.count]);
        that.nextQuestion();
      }
      that.timer--;
      if (that.count == that.length) {
        clearInterval(timer);
        that.showResult();
      }
    }, 1000, that)

  },

  checkAnswer: function(answer, selectedAnswer) {
    console.log(answer + ' > ' + selectedAnswer);
    if (answer == selectedAnswer) {
      this.total++;
    }
    this.nextQuestion();
  },

  showResult: function() {
    $('#content').hide();
    $('#result').html('<h2>Your total score is: ' + this.total + '</h2>');
    $('#restart').show();
  },

  printQuestion: function(data) {
    if (data) {
      $('#question').html(data.question);
      $($('ul#choices li')[0]).find('a').text(data.choices.a);
      $($('ul#choices li')[1]).find('a').text(data.choices.b);
      $($('ul#choices li')[2]).find('a').text(data.choices.c);
      $($('ul#choices li')[3]).find('a').text(data.choices.d);
    }
  }
}

$(document).ready(function() {
  settings.init();

  $('#start').on('click', function(e) {
    e.preventDefault();
    $('#content').show();
    settings.startRound();
    $(this).remove();
  });

  $('a.choice').on('click', function(e) {
    e.preventDefault();
    var currentQuestion = questions[settings.count];

    var yourAnswer = $(this).attr('data-choice');
    var answer = currentQuestion.answer;
    settings.checkAnswer(answer, yourAnswer);
  });
  
  $('#restart').on('click',function(e){
    e.preventDefault();
    $('#content').show();
    $('#result').hide();
    settings.startRound();
    $(this).remove();
  });
});