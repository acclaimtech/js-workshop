var questions = [{
  question: 'Brass gets discoloured in air because of the presence of which of the following gases in air?',
  choices: {
    a: 'a) Oxygen',
    b: 'b) Hydrogen sulphide',
    c: 'c) Carbon dioxide',
    d: 'd) Nitrogen'
  },
  answer: 'b'
}, {
  question: 'Which of the following is a non metal that remains liquid at room temperature?',
  choices: {
    a: 'a) Phosphorous',
    b: 'b) Bromine',
    c: 'c) Chlorine',
    d: 'd) Helium'
  },
  answer: 'b'
}, {
  question: 'Which of the following is used as a moderator in nuclear reactor?',
  choices: {
    a: 'a) Thorium',
    b: 'b) Radium',
    c: 'c) Graphite',
    d: 'd) Ordinary water'
  },
  answer: 'c'
}, {
  question: 'Which type of fire extinguisher is used for petroleum fire?',
  choices: {
    a: 'a) Powder type',
    b: 'b) Liquid type',
    c: 'c) Soda acid type',
    d: 'd) Foam type'
  },
  answer: 'a'
}, {
  question: 'Who invented the BALLPOINT PEN?',
  choices: {
    a: 'a) Write Brothers',
    b: 'b) Waterman Brothers',
    c: 'c) Bicc Brothers',
    d: 'd) Biro Brothers'
  },
  answer: 'd'
}];