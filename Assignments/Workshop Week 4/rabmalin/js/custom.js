( function( $ ) {

    var interval_id = null;
    var answers_given = [];
    var i = 0;
	var my_data = new Array();

	$( document ).ready(function($){

        // Hide submit button at first.
        $('#btn-submit').prop('disabled',true).hide();

        $('#btn-submit').click(function(e){
            update_answer();
            e.preventDefault();
            if ( i == my_data.length ) {
                clearInterval(interval_id);
                display_results();
                $(this).hide();
                return;
            }

            clearInterval(interval_id);
            render_repeat();
            interval_id = setInterval( render_repeat, 5000 );
        });

        function update_answer() {
        	if(true == check_duplicate( $('#question-title').html() ) ){
        		return;
        	}
        	var temp_obj = new Object();
        	temp_obj.question = $('#question-title').html();
        	temp_obj.answer = $('input:radio[name=answer]:checked').val();
        	temp_obj.correct = $('#question-title').data('ans');
        	answers_given.push( temp_obj );
        }

        function check_duplicate( key ) {
        	var status = false;
        	if ( answers_given ) {
        		for( var cnt = 0; cnt < answers_given.length; cnt++ ){
        			if ( key == answers_given[cnt].question ) {
        				status = true;
        				break;
        			}

        		}
        	}
        	return status;
        }

		$('#btn-start').click(function(e){
			e.preventDefault();
			$(this).prop('disabled',true).hide();

			var api_url = "http://localhost/online-test/data.json";
			$.getJSON( api_url )
			.done(function( data ) {
				if( data ) {
					assign_data(data);
				}
				else {
					alert('Error');
				}
			});
		});

        function assign_data(data){
            my_data = data;
            render_main();
        }

		function render_main(){
            render_single_question_item(my_data[i++]);
            interval_id = setInterval( render_repeat, 5000 );
		}

        function render_repeat(){
            update_answer();
            if ( i == my_data.length ) {
                clearInterval(interval_id);
                display_results();
                return;
            }
            render_single_question_item(my_data[i++]);
        }

        function display_results() {

        	var score = 0;
            var markup = '<table>';
            markup += '<tr>';
            markup += '<td>Question</td>';
            markup += '<td>Answer</td>';
            markup += '<td>Your Answer</td>';
            markup += '</tr>';
            for ( var c = 0; c < answers_given.length; c++ ) {
	            markup += '<tr>';
	            markup += '<td>' + answers_given[c].question + '</td>';
	            markup += '<td>' + answers_given[c].correct + '</td>';
	            var user_answer = '';
	            if ( answers_given[c].answer ) {
	            	user_answer = answers_given[c].answer;
	            }
	            markup += '<td>' + user_answer + '</td>';
	            markup += '</tr>';
	            if ( user_answer == answers_given[c].correct ) {
	            	score++;
	            }
            }
            markup += '</table>';
            markup += '<div class="score">Score: ' + score + '</div>';
            $('#questions').hide();
            $('#results').html(markup).fadeIn('slow');

        }

        function render_single_question_item( q ) {
            if( ! q ) {
                return;
            }
            $('#question-title').html(q.question).data('ans', q.answer);
            var ans = '';
            for( var j = 0; j < q.answers.length; j++ ) {
                ans += '<input type="radio" name="answer" value="' + q.answers[j] + '">' + q.answers[j] + '<br/>';
            }
            $('#question-answers').html(ans);
            $('#btn-submit').prop('disabled',false).show();
        }

	});

} )( jQuery );
