<!doctype>
<html>
<head>
<meta charset="utf-8"/>
<title> Javascript workshop assignment 2 : Digital clock  </title>
<link href='https://fonts.googleapis.com/css?family=Fjalla+One' rel='stylesheet' type='text/css'>
<style>
div.table-wrapper{
	width: 400px;
    height: auto;
    background-color: #CAAFAF;
    padding: 10px;
}
.table-container{
	background-color: rgb(144, 113, 160);
    border-radius: 15px;
    margin: 10px auto;
    width: 100%
}
table{
    border: 1px solid black;
    width: 100%;
    margin: 10px;
}
#timecell{
	font-weight: bold;
	font-size: 30px;
	text-align: center;
	color: red;

}

</style>

</head>
<body>
<script type="text/javascript">
function show_digital_clock(){
	
	var now, time, hr, min, sec, shift, month, months, year, day, days;
	days = ["Sun","Mon", "Tue", "Wed", "Thu","Fri","Sat"];
	months = [ "Jan","Feb","Mar","Apr","May","jun","Jul","Aug","Sep","Oct","Nov","Dec"];
	now = new Date();
	hr = now.getHours();
	min = now.getMinutes();
	sec = now.getSeconds();
	time = hr + ":" +min + ":" +sec;
	document.getElementById( 'timecell' ).innerHTML = time;

	if( hr < 12 ){
		document.getElementById( 'meridian' ).innerHTML = 'AM'
	}
	else{
		document.getElementById( 'meridian' ).innerHTML = 'PM'
	}
	day = now.getDay();
	n  = now.getDate();
	month = now.getMonth();
	year  =now.getFullYear();
	document.getElementById( 'date' ).innerHTML = days[day] + " " +n + " " + months[month]+ " " + year ;
	setInterval( 'show_digital_clock()', 1000 );
}
// window.onload = show_digital_clock();
</script>
<div class="table-wrapper">
	<div class="table-container">
		<table class="digital_table">
			<tr>
				<td id="timecell" rowspan="2">Time</td>
				<td id="meridian" >AM / PM </td>
			</tr>
			<tr>
				<td id="date">Date</td>
			</tr>
		</table>
	</div><!-- .table-container -->
</div><!-- .table-wrapper -->
<button onclick="show_digital_clock()">Click me</button>
</script>
</body>
</html>