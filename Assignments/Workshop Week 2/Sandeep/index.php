<!doctype>
<html>
<head>
<meta charset="utf-8"/>
<title> Javascript workshop learning </title>
<style>
#container{
	width: 500;
	height: 500;
	background-color: green;
	position: relative;
}
#box{
	width: 100px;
	height: 100px;
	background-color: gray;
	position: absolute;
}
</style>
</head>
<body>
<script type="text/javascript">
	function get_values(){
		var i, form1, display;
		display = "";
		form1 = document.forms['record'];
		for( i = 0; i < form1.length; i++ ){
			display += form1.elements[i].value + '<br>';
		}
		document.getElementById( "output" ).innerHTML = display;
	}
</script>
<form id="record" action="#">
	<label> First name = </label> <input type="text" name="fname" value="fname">
	<label> Last name = </label> <input type="text" name="lname" value="lname">
	<label> age = </label> <input type="text" name="age" value="age">
	<input type="submit" value="Submit" onclick="get_values()">
</form>
	<p id="output"> Click submit to get values from form </p>
<h1 align="centre"> Simple animation using js </h1>

<div id="container">
	<div id="box">
	</div>
</div>
<button onclick="animate()">Animate </button>

<script type="text/javascript">

	function animate(){
		var square = document.getElementById('box');
		var pos  = 0;
		var time = setInterval( frame, 10 );
		function frame(){
			if( pos == 400 ){
				clearInterval( time );
			}
			else{
				pos ++;
				square.style.top  = pos + "px";
				square.style.left = pos + "px";
			}
		}
	}
</script>
</body>
</html>