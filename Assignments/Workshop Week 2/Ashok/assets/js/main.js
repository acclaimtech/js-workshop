
function getCurrentTime(){
    var today = new Date(),
        hour     = today.getHours(),
        min      = today.getMinutes(),
        sec      = today.getSeconds();
    min = makeTwoDigit(min);
    sec = makeTwoDigit(sec);
    return  hour + ":" + min + ":" + sec;
}

function makeTwoDigit(time) {
    if (time < 10) {
        time = "0" + time;
    }
    return time;
}

setInterval(function(){
    document.getElementById('curr_time').innerHTML = getCurrentTime();
},500);

function add(){
    var num1 =  document.getElementById('num1').value,
        num2 =  document.getElementById('num2').value,
        result = 0;
        if(!isNaN(num1) && !isNaN(num2)){
            result = parseFloat(num1) + parseFloat(num2);    
        } else {
            result = "Only numbers!!!"
        }
        
    document.getElementById('result').innerHTML = result;
    return false;
    
}

function clone(obj){
    document.getElementById('response_result').innerHTML = obj.value;
}