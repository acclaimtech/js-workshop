var app = angular.module('attendeeApp', []);
app.controller('attendeeController', function($scope, $http) {
    $http.get("http://tech.abiralneupane.com.np/wp-json/js-workshop/attendees")
    .then(function(response) {
        $scope.attendees = response.data;
    });
    $scope.submit_attendee = function(){
        var attendee_name = $('#attendee-name').val();
        var attendee_position = $('#attendee-position').val();
        var attendee_company = $('#attendee-company').val();

        var obj = new Object();
        obj.name = attendee_name;
        obj.position = attendee_position;
        obj.company = attendee_company;
        var api_url = "http://tech.abiralneupane.com.np/wp-json/js-workshop/attendee";
        $http({
            method  : 'POST',
            url     : api_url,
            data    : $.param(obj),
            headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
        })

    }
});
