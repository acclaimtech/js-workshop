var table = document.getElementById('result');
   
fetchAll();

function fetchAll(){
    $('#progress').html(loader());
    // clearRows();
    var table_row = table.rows.length;
    $.ajax({
        url: 'http://tech.abiralneupane.com.np/wp-json/js-workshop/attendees',
        type:'GET',
        dataType:'JSON',
        crossDomain : true,
        success:function(res){
            $('#progress').html('');
            if(res){
                for (var i = 0; i <= res.length; i++ ) {
                        var row = table.insertRow( table_row + i );
                         insert(row,res[i]);
                }
            } // if
        }
    });   
}

function addEmp(){
    // e.preventDefault();
    var name = document.getElementById('name').value,
        position = document.getElementById('position').value,
        company = document.getElementById('company').value,
        table_row = table.rows.length;
       
        
        $.ajax({
            url: 'http://tech.abiralneupane.com.np/wp-json/js-workshop/attendee',
            type:'POST',
            data:{name:name,position:position,company:company},
            dataType:'JSON',
            crossDomain : true,
             beforeSend: function(){
                $('#progress').html(loader());
            },
            success:function(res){
                $('#progress').html('');
                var row = table.insertRow( table_row );
                    insert(row,res[0]);
                    clearField();
            }
        });
        return false;
}

function insert(row,obj){
    row.insertCell(0).innerHTML = obj.name;
    row.insertCell(1).innerHTML = obj.position;
    row.insertCell(2).innerHTML = obj.company;
}

function loader(){
    var url = 'http://js-workshop-ashokmhrj.c9users.io/Assignments/Workshop Week 3/Ashok/assets/images/loader.png';
    return '<img src="' + encodeURI(url) + '" />';
}

function searchEmp(){
     var search = document.getElementById('search').value;
     if( isNaN(search) ){
         alert('Only Employee ID');
         return;
     }
      $.ajax({
            url: 'http://tech.abiralneupane.com.np/wp-json/js-workshop/attendee/'+search,
            type:'GET',
            data:{},
            dataType:'JSON',
            crossDomain : true,
            beforeSend: function(){
                $('#progress').html(loader());
            },
            success:function(res){
                $('#progress').html('');
                if( res.message ){
                    alert(res.message);
                    return;
                }
                var msg = "Name: " + res.name;
                    msg += "\nPosition: " + res.position;
                    msg += "\nCompany: " + res.company;
                    alert(msg);
                document.getElementById('search').value='';
            }
        });
        return false;
     
     
}

function clearField(){
    document.getElementById('name').value='';
    document.getElementById('position').value='';
    document.getElementById('company').value='';
}

function clearRows(){
     var table_row = table.rows.length;
     if(table_row <= 0 ) { 
         return;
     }
     while( table_row <= 0 ){
      document.getElementById("result").deleteRow(table_row--);
     }
}