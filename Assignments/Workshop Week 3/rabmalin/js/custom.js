( function( $ ) {

	$( document ).ready(function($){

		// Saving data.
		$('#btn-save').click(function(e){
			e.preventDefault();
			var attendee_name = $('#attendee-name').val();
			var attendee_position = $('#attendee-position').val();
			var attendee_company = $('#attendee-company').val();
			if ( ! attendee_name || ! attendee_company || ! attendee_position ) {
				alert('Please fill up all inputs');
				return false;
			}
			var obj = new Object();
			obj.name = attendee_name;
			obj.position = attendee_position;
			obj.company = attendee_company;

			var api_url = "http://tech.abiralneupane.com.np/wp-json/js-workshop/attendee";
			$.post( api_url, obj )
			.done(function( data ) {
				if( data ) {
					$('#attendee-name').val('');
					$('#attendee-position').val('');
					$('#attendee-company').val('');
					var additional_markup = getRowMarkup( data[0] );
					$('#data-area').prepend( additional_markup );
				}
				else {
					alert( 'Error' );
				}
			});
		});

		function isValidInteger(str) {
		    var n = ~~Number(str);
		    return String(n) === str && n >= 0;
		}


		// Clear.
		$('#btn-clear').click(function(e){
			e.preventDefault();
			$('#s').val('');
			$('#search-content').html('');
		});


		// Search.
		$('#btn-search').click(function(e){
			e.preventDefault();
			var search_key = $('#s').val();
			if ( ! search_key ) {
				alert('Please enter ID');
				return false;
			}
			if ( ! isValidInteger( search_key )  ) {
				alert('Invalid input');
				return false;
			}

			var api_url = "http://tech.abiralneupane.com.np/wp-json/js-workshop/attendee";
			api_url += '/' + search_key;
			$.getJSON( api_url )
			.done(function( data ) {
				if( data ) {
					var additional_markup = '';
					additional_markup += '<p><span class="field-key">Name</span>' + data.name + '</p>';
					additional_markup += '<p><span class="field-key">Position</span>' + data.position + '</p>';
					additional_markup += '<p><span class="field-key">Company</span>' + data.company + '</p>';
					$('#search-content').html( additional_markup );
				}
				else {
					$('#search-content').html( '<strong>Not found</strong>' );
				}
			});
		});

		// Rendering.
		renderEmployeeData();

		function getRowMarkup(obj) {
			var output = '';
			output += '<div class="row">';
			output += '<span class="attendee-id">' + obj.id + '</span>';
			output += '<span class="attendee-name">' + obj.name + '</span>';
			output += '<span class="attendee-position">' + obj.position + '</span>';
			output += '<span class="attendee-company">' + obj.company + '</span>';
			output += '</div>';
			return output;
		}
		function renderEmployeeData() {
			$('.loading').show();
			var api_url = "http://tech.abiralneupane.com.np/wp-json/js-workshop/attendees";
			$.getJSON( api_url )
			.done(function( data ) {
				$('.loading').hide();
				var markup = '';
				$.each( data, function( i, item ) {
				    markup += getRowMarkup( item );
				});
				$('#data-area').html(markup);
			});

		}

	});

} )( jQuery );
