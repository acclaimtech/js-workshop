<html>
<meta charset="UTF-8"> 
	<head>	
	<title> Fetching data from record.json </title>
	<script src="https://code.jquery.com/jquery-1.10.2.js"></script> 
	</head>
	<body>
	<p id="placeholder" ></p>
	<script>

	  	jQuery(function($){ 
	  		var data = {
						"employee": [
							{
							"fname"   : "Sandeep Regmi",
							"company" : "Theme palace",
							"position": "Theme developer",
							"contact" : {
								"address": "basundhara",
								"phone"  : "9843647569",
							}
							},

							{
							"fname"   : "Sachet Gurung",
							"company" : "Theme palace",
							"position": "Theme developer", 
							"contact" : {
								"address": "bansbari- chakrapath",
								"phone"  : "98045632145",
							}
							},
						]
					};
		 // $.getJSON('record.json', function( record ) {
		  	//console.log('test');
		  		// var data = JSON.parse(record);
		        var output="<ul>";
		        for ( var i in data.employee) {
		            output+="<li>" + data.employee[i].fname + " " + data.employee[i].company + "--" + data.employee[i].position+" " +data.employee[i].contact["address"]+ "--"+data.employee[i].contact["phone"]+"</li>";
		        }

		        output+="</ul>";
		        document.getElementById('placeholder').innerHTML= output;
		  });
    </script>

	</body>
</html>

