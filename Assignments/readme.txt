Follow following steps to upload your assignments
1. Open respective workshop day folder. Eg: "Workshop week 2"
2. Create subfolder with your name. Eg: "John Doe"
3. Upload your assignment there.